module.exports = {
  siteMetadata: {
    title: 'Nomava',
    titleTemplate: '%s · Real African stories',
    description:
      'The story of Africa told from an African perspective. Nomava is an encyclopedia for African knowledge. Signup now and Join the African revolution.',
    siteUrl: 'https://nomava.co.za', // No trailing slash allowed!
    image: '/static/assets/layer.png', // Path to your image you placed in the 'static' folder
    twitterUsername: '@bqwabi',
  },
  plugins: [
    `gatsby-plugin-netlify-cms`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/static/assets`,
        name: `assets`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/article`,
        name: 'markdown-pages',
      },
    },
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `limelight`,
          `Ubuntu\:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700`,
          `source sans pro\:300,400,400i,700`, // you can also specify font weights and styles
        ],
        display: 'swap',
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
  ],
};
