---
path: /article/nguni-human-word
date: 2020-06-16T11:28:16.158Z
title: The Nguni word for "Human"
thumbnail: /assets/cowrie-shells.jpg
brief: >-
  The Nguni people do not have a scientific term for describing a human being
  but rather  a combination of words that describe the ethnic group a person
  belongs to
---
The Zulu, Xhosa and Ndebele people of South Africa are commonly referred to as the Nguni people.They are called Nguni because they share the same ancestor; Mnguni. Mnguni was a descendant of Ntu who lived near the equatorial center of Africa Ethiopia. It is said that his descendants migrated towards the Southern parts of Africa

These three tribes share similarities on their way of speaking. This makes it easier to understand words and phrases in either Xhosa, Zulu or Ndebele. A common word that is  shared amongst these three ethnic groups is the word "Umntu" which translates to "Human" or "Person".

![](/assets/1543999327g48nk.jpg)

Now my theory is that this word doesn't translate to any of these two words  but rather translates to the phrase "Descendant of Ntu"

Even though the word "Umntu" has been used to describe a person, it's origins say otherwise.

## Argument

The letter "u" in Nguni languages is commonly used as an adjective, meaning that it describes the object it is pointing to

if we say "ukutya" or "u-kutya" we are simply saying "food" in the English language but in essence food is "kutya". The letter "u" is used as a prefix to tell us what the object is  in the Nguni languages.

When we say "umXhosa" or  "umZulu" we are describing that the person being referred to is a descedenat of Xhosa or a descendant of Zulu or   alternatively we could being saying that the person is "of the Zulu tribe" or "of the Xhosa tribe".

Now because we are descendants of Nguni who is a descendant of Ntu that means our language comes from the original language our common ancestor spoke. So when we say "umntu" or "umNtu" we are simply saying that the person is a descendant of Ntu or we are alternatively saying that the person is "of the Ntu tribe". 

This then means that the Nguni nation does not specifically have an equivalent term for the word "Human" but the word "umntu" is used in its place.

![](/assets/if.png)

## Conclusion

This may seem like a far fetched theory, but according to the way the Nguni have been structuring words it is clearly evident that the word "umntu" simply describes the the person is a descendant of Ntu instead of describing the scienticfic features of such, like the word Human.

The topic is open for debate so please feel free to leave a comment below.

Article Written by Ayabonga Qwabi
