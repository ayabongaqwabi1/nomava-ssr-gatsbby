import React from 'react';
import PropTypes from 'prop-types';
import { FacebookProvider } from 'react-facebook';
export default function HTML(props) {
  return (
    <html {...props.htmlAttributes}>
      <head>
        {props.headComponents}
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        {/* <script
          src="https://contextual.media.net/dmedianet.js?cid=8CU234270"
          async="async"
        /> */}
      </head>
      <body {...props.bodyAttributes}>
        <FacebookProvider appId="180799756618212">
          {props.preBodyComponents}
          <noscript key="noscript" id="gatsby-noscript">
            This app works best with JavaScript enabled.
          </noscript>
          <div
            key={`body`}
            id="___gatsby"
            dangerouslySetInnerHTML={{ __html: props.body }}
          />
          {props.postBodyComponents}
          {/* <script
            async
            src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
          ></script>
          <script>
            {`(adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-5272613462144219",
            enable_page_level_ads: true
          });`}
          </script>
          <script
            data-ad-client="ca-pub-8294995671791919"
            async
            src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
          ></script> */}
        </FacebookProvider>
      </body>
    </html>
  );
}

HTML.propTypes = {
  htmlAttributes: PropTypes.object,
  headComponents: PropTypes.array,
  bodyAttributes: PropTypes.object,
  preBodyComponents: PropTypes.array,
  body: PropTypes.string,
  postBodyComponents: PropTypes.array,
};
