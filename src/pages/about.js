import React, { Component } from "react"
import * as R from "ramda"
import african from "../../static/assets/african.svg"
import layer from "../../static/assets/layer.png"
import "./style.scss"
import PageSeo from "../components/PageSeo"
import Layout from "../components/Layout"

export default class articleIndex extends Component {
  render() {
    const seo = {
      title: "About Nomava",
      description: "Find out information about the Nomava project.",
      pathname: "/about",
    }
    return (
      <Layout seo={seo}>
        <div>
          <div className="text-center">
            <div className="story with-disclaimer">
              <br />
              <br />
              <h1>The story of Africa</h1>
              <br />
              <h4>
                For generations the story of Africa has been told by
                non-Africans. This initiative hopes to inspire Africans to come
                together to create a centrified location of all African
                knowledge.
              </h4>
              <br />
              <br />
              <h4>
                In history all African knowledge has been passed down by word of
                mouth meaning most of our knowledge has got lost through time
                because a great aunt maybe forgot to instill knowledge upon her
                children. A Africans we have strongly held on to this method of
                knowledge sharing. The modern age has provided us with the tools
                and resources to store our collective knowledge in written form
                so as to preserve who we are.
              </h4>
              <br />
              <h4>
                <strong>The Team</strong>
              </h4>
              <h4>
                Nomava is desgined, built and developed by leading pioneers in
                web development industry; Midas Touch Technologies
                (www.touch.net.za)
              </h4>
              <br />
              <p>
                <strong>Ayabonga Qwabi</strong>
              </p>
              <p>Developer</p>
              <br />
            </div>
            <img
              className="floating-left hide-mobile"
              src={R.isNil(african) ? "" : layer}
              alt="illustration"
            />
          </div>
        </div>
      </Layout>
    )
  }
}
