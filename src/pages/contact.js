import React, { Component } from "react"
import * as R from "ramda"
import african from "../../static/assets/african.svg"
import layer from "../../static/assets/layer.png"
import "./style.scss"
import PageSeo from "../components/PageSeo"
import ReCAPTCHA from "react-google-recaptcha"
import Layout from "../components/Layout"

export default class articleIndex extends Component {
  render() {
    const seo = {
      title: "Contact Nomava",
      description:
        "Find out the contact details of the Nomava project including emails and cellphone numbers.",
      pathname: "/contact",
    }
    return (
      <Layout seo={seo}>
        <div>
          <div className="text-center">
            <div className="story with-disclaimer contact">
              <br />
              <h1>Contact</h1>
              <h4>
                If you have any article ideas or you have feedback on one of our
                articles you can email us at{" "}
                <a href="mailto:articles@nomava.co.za">articles@nomava.co.za</a>
                <br />
                If you have noticed something strange with our website please
                notify our tech team at{" "}
                <a href="mailto:nomava@touch.net.za">nomava@touch.net.za</a>
              </h4>
              <h4>
                Call or Whatsapp with us at{" "}
                <a href="tel:+27672023083">+27672023083</a>
              </h4>
              <h4> Or simply drop us a message and we'll get back to you </h4>
              <form
                name="JSX Form"
                method="POST"
                data-netlify="true"
                data-netlify-recaptcha="true"
                className="ui form text-left"
                action="/thankyou"
              >
                <input type="hidden" name="form-name" value="JSX Form" />
                <div class="field is-horizontal">
                  <div class="field-label is-normal mt-5">
                    <label class="label">Email</label>
                  </div>
                  <div class="field-body">
                    <div class="field">
                      <p class="control">
                        <input
                          class="input"
                          type="email"
                          name="email"
                          placeholder="email@example.com"
                        />
                      </p>
                    </div>
                  </div>
                </div>
                <div class="field is-horizontal">
                  <div class="field-label is-normal mt-5">
                    <label class="label">Fullname</label>
                  </div>
                  <div class="field-body">
                    <div class="field">
                      <p class="control">
                        <input
                          class="input"
                          type="text"
                          name="name"
                          placeholder="Your fullname"
                        />
                      </p>
                    </div>
                  </div>
                </div>
                <br />
                <div class="field is-horizontal">
                  <div class="field-label is-normal mt-5">
                    <label class="label">Message</label>
                  </div>
                  <div class="field-body">
                    <div class="field">
                      <p class="control">
                        <textarea
                          class="textarea"
                          placeholder="Your message here"
                          rows="5"
                        />
                      </p>
                    </div>
                  </div>
                </div>
                <br />
                <div className="has-text-centered">
                  <ReCAPTCHA
                    sitekey={
                      process.env.GATSBY_RECAPTCHA_KEY ||
                      "6Leqpv0UAAAAABM9Gs2cSnn_sqyh9M4bZ7cS-xZH"
                    }
                  />
                </div>
                <br />
                <button class="button is-primary" type="submit">
                  Send
                </button>
                <br />
              </form>
            </div>
            <img
              className="floating-left with-discalaimer"
              src={R.isNil(african) ? "" : layer}
              alt="illustration"
            />
          </div>
        </div>
      </Layout>
    )
  }
}
