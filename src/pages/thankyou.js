import React, { Component } from "react"
import "./style.scss"
import PageSeo from "../components/PageSeo"
import BurgerMenu from "../components/menu"
export default class articleIndex extends Component {
  render() {
    return (
      <div>
        <PageSeo />
        <div className="text-center">
          <div className="row">
            <div className="col-md-6 offset-6 nav">
              <BurgerMenu />
            </div>
          </div>
          <div className="story with-disclaimer">
            <h1>Contact</h1>
            <p>Thank you for your submission!</p>
          </div>
        </div>
      </div>
    )
  }
}
