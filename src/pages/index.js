import React, { Component } from 'react';
import { graphql } from 'gatsby';
import { navigateTo } from 'gatsby-link';
import { last, splitEvery, contains, isEmpty } from 'ramda';
import layer from '../../static/assets/layer.png';
import './style.scss';
import Fuse from 'fuse.js';
import Layout from '../components/Layout';
import { event } from 'react-ga';

const text_truncate = function(str, length, ending) {
  if (length == null) {
    length = 100;
  }
  if (ending == null) {
    ending = '...';
  }
  if (str.length > length) {
    return str.substring(0, length - ending.length) + ending;
  } else {
    return str;
  }
};

export default class articleIndex extends Component {
  constructor() {
    super();
    this.state = { results: [], showResults: false };
    this.onChange = this.onChange.bind(this);
  }

  componentWillMount() {
    const articles = this.props.data.allMarkdownRemark.edges;
    const cleanArticles = articles.map((article) => {
      const { title, brief, thumbnail, date, path } = article.node.frontmatter;
      return { title, brief, thumbnail, date, path };
    });
    var options = {
      shouldSort: true,
      threshold: 0.6,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: ['title', 'brief'],
    };
    var fuse = new Fuse(cleanArticles, options);
    this.setState({ fuse });
  }

  onChange(e) {
    const val = e.target.value;
    this.setState({ results: this.state.fuse.search(val), showResults: true });
  }

  render() {
    const { showResults, results } = this.state;
    const articles = this.props.data.allMarkdownRemark.edges;
    const imageSharps = this.props.data.allImageSharp.edges;
    const cleanArticles = articles.map((article) => {
      const { title, brief, thumbnail, date, path } = article.node.frontmatter;
      const imgNode = imageSharps.filter((node) => {
        const { originalName } = node.node.fluid;
        return contains(originalName, thumbnail);
      });
      const fluid = !isEmpty(imgNode) ? imgNode[0]['node']['fluid'] : {};
      return { title, brief, fluid, date, path };
    });
    const sortedArticles = cleanArticles
      .slice()
      .sort((a, b) => b.date - a.date);
    const latest = last(splitEvery(3, sortedArticles));
    return (
      <Layout>
        <div id="outer-container">
          <div className="text-center">

            <div className="story" id="story">
              <h1> The story of Africa </h1>
              <h4>told from an African perspective </h4>
              <br />
              <div className="field">
                <div className="control">
                  <input
                    className="input is-large"
                    type="text"
                    placeholder="Search..."
                    onChange={this.onChange}
                  />
                </div>
              </div>
              {showResults && (
                <div className="row text-center">
                  <div className="results-box column is-full">
                    {results.map((result) => {
                      const { title, path, brief } = result;
                      return (
                        <div
                          className="row result"
                          onClick={() => {
                            event({
                              category: 'User',
                              action: 'View search result',
                              label: title,
                              value: path,
                            });
                            navigateTo(path);
                          }}
                        >
                          <div className="column is-full text-left">
                            <strong>{title}</strong>
                          </div>
                          <div className="column is-full text-left">
                            <small>{text_truncate(brief, 70)}</small>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              )}
              <div className="grid">
                <span class="tag is-warning">Latest</span>
                {latest.map((a) => {
                  return (
                    <div className="grid__item">
                      <div className="card">
                        <img className="card__img" src={a.fluid.srcWebp} />
                        <div className="card__content">
                          <h2 className="card__header">{a.title}</h2>
                          <p className="card__text">
                            {text_truncate(a.brief, 90, '...')}
                          </p>
                          <a href={a.path}>
                            <button className="card__btn">Read</button>
                          </a>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
            <img className="floating-left" src={layer} alt="illustration" />
          </div>
        </div>
      </Layout>
    );
  }
}

export const pageQuery = graphql`
  query assetsAndIndexPage {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: ASC }) {
      edges {
        node {
          frontmatter {
            title
            date
            path
            date
            thumbnail
            brief
          }
        }
      }
    }
    allImageSharp {
      edges {
        node {
          id
          fluid(maxHeight: 200, maxWidth: 200) {
            base64
            tracedSVG
            srcWebp
            srcSetWebp
            originalImg
            originalName
          }
        }
      }
    }
  }
`;
