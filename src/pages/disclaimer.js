import React, { Component } from "react"
import * as R from "ramda"
import african from "../../static/assets/african.svg"
import layer from "../../static/assets/layer.png"
import "./style.scss"
import PageSeo from "../components/PageSeo"
import Layout from "../components/Layout"

export default class articleIndex extends Component {
  render() {
    const seo = {
      title: "Disclaimer",
      description: "Read the disclaimer terms of the Nomava project.",
      pathname: "/disclaimer",
    }
    return (
      <Layout seo={seo}>
        <div>
          <div className="text-center">
            <div className="row">
              <div className="col-md-6 offset-6 nav"></div>
            </div>
            <div className="story with-disclaimer">
              <h1> The story of Africa</h1>
              <h4>
                <i>told from an African perspective</i>
              </h4>
              <br />
              <div className="text-left col-md-12 disclaimer-text">
                <h5>WEBSITE DISCLAIMER</h5>
                <p>
                  The information provided by Midas Touch Technologies
                  (“Company”, “we”, “our”, “us”) on nomava.co.za (the “Site”) is
                  for general informational purposes only. All information on
                  the Site is provided in good faith, however we make no
                  representation or warranty of any kind, express or implied,
                  regarding the accuracy, adequacy, validity, reliability,
                  availability, or completeness of any information on the Site.
                  UNDER NO CIRCUMSTANCE SHALL WE HAVE ANY LIABILITY TO YOU FOR
                  ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE USE
                  OF THE SITE OR RELIANCE ON ANY INFORMATION PROVIDED ON THE
                  SITE. YOUR USE OF THE SITE AND YOUR RELIANCE ON ANY
                  INFORMATION ON THE SITE IS SOLELY AT YOUR OWN RISK.
                </p>
                <h5>EXTERNAL LINKS DISCLAIMER</h5>
                <p>
                  The Site may contain (or you may be sent through the Site)
                  links to other websites or content belonging to or originating
                  from third parties or links to websites and features. Such
                  external links are not investigated, monitored, or checked for
                  accuracy, adequacy, validity, reliability, availability or
                  completeness by us. For example, the outlined Disclaimer has
                  been created using PolicyMaker.io, a free web application for
                  generating high-quality legal documents. PolicyMaker’s free
                  website disclaimer generator is an easy-to-use tool for
                  creating an excellent sample Disclaimer template for a
                  website, blog, eCommerce store or app. WE DO NOT WARRANT,
                  ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR THE ACCURACY
                  OR RELIABILITY OF ANY INFORMATION OFFERED BY THIRD-PARTY
                  WEBSITES LINKED THROUGH THE SITE OR ANY WEBSITE OR FEATURE
                  LINKED IN ANY BANNER OR OTHER ADVERTISING. WE WILL NOT BE A
                  PARTY TO OR IN ANY WAY BE RESPONSIBLE FOR MONITORING ANY
                  TRANSACTION BETWEEN YOU AND THIRD-PARTY PROVIDERS OF PRODUCTS
                  OR SERVICES.
                </p>
                <h5>PROFESSIONAL DISCLAIMER</h5>
                <p>
                  The Site can not and does not contain legal advice. The
                  information is provided for general informational and
                  educational purposes only and is not a substitute for
                  professional legal advice. Accordingly, before taking any
                  actions based upon such information, we encourage you to
                  consult with the appropriate professionals. We do not provide
                  any kind of legal advice. Content published on nomava.co.za is
                  intended to be used and must be used for informational
                  purposes only. It is very important to do your own analysis
                  before making any decision based on your own personal
                  circumstances. You should take independent legal advice from a
                  professional or independently research and verify any
                  information that you find on our Website and wish to rely
                  upon. THE USE OR RELIANCE OF ANY INFORMATION CONTAINED ON THIS
                  SITE IS SOLELY AT YOUR OWN RISK.{" "}
                </p>
                <h5>AFFILIATES DISCLAIMER</h5>{" "}
                <p>
                  {" "}
                  The Site may contain links to affiliate websites, and we may
                  receive an affiliate commission for any purchases or actions
                  made by you on the affiliate websites using such links.
                </p>
                <h5>ERRORS AND OMISSIONS DISCLAIMER</h5>{" "}
                <p>
                  {" "}
                  While we have made every attempt to ensure that the
                  information contained in this site has been obtained from
                  reliable sources, Midas Touch Technologies is not responsible
                  for any errors or omissions or for the results obtained from
                  the use of this information. All information in this site is
                  provided “as is”, with no guarantee of completeness, accuracy,
                  timeliness or of the results obtained from the use of this
                  information, and without warranty of any kind, express or
                  implied, including, but not limited to warranties of
                  performance, merchantability, and fitness for a particular
                  purpose. In no event will Midas Touch Technologies, its
                  related partnerships or corporations, or the partners, agents
                  or employees thereof be liable to you or anyone else for any
                  decision made or action taken in reliance on the information
                  in this Site or for any consequential, special or similar
                  damages, even if advised of the possibility of such damages.
                </p>{" "}
                <h5>GUEST CONTRIBUTORS DISCLAIMER </h5>
                <p>
                  {" "}
                  This Site may include content from guest contributors and any
                  views or opinions expressed in such posts are personal and do
                  not represent those of Midas Touch Technologies or any of its
                  staff or affiliates unless explicitly stated.
                </p>{" "}
                <h5>LOGOS AND TRADEMARKS DISCLAIMER </h5>
                <p>
                  {" "}
                  All logos and trademarks of third parties referenced on
                  nomava.co.za are the trademarks and logos of their respective
                  owners. Any inclusion of such trademarks or logos does not
                  imply or constitute any approval, endorsement or sponsorship
                  of Midas Touch Technologies by such owners. CONTACT US Should
                  you have any feedback, comments, requests for technical
                  support or other inquiries, please contact us by email:
                  nomava@touch.net.za.
                </p>
              </div>
            </div>
            <img
              className="floating-left with-discalaimer"
              src={R.isNil(african) ? "" : layer}
              alt="illustration"
            />
          </div>
        </div>
      </Layout>
    )
  }
}
