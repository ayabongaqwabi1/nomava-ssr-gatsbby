import React, { Component } from "react"
import { graphql } from "gatsby"
import PostItem from "../../components/PostItem"
import { navigateTo } from "gatsby-link"
import Layout from "../../components/Layout"
import * as R from "ramda"
import african from "../../../static/assets/african.svg"
import "./style.scss"
import ReactGA from "react-ga"

export default class articleIndex extends Component {
  render() {
    const articles = this.props.data.allMarkdownRemark.edges.reverse()
    return (
      <Layout>
        <div className="has-text-centered">
          <div className="column is-half text-left back-to-home">
            <button
              class="button is-danger is-outlined"
              onClick={() => navigateTo("/")}
            >
              Home
            </button>
          </div>
          <br />
          <h1 className="is-size-3">The story of Africa</h1>
          <br />
          {articles.map(article => {
            console.log(article)
            const {
              title,
              brief,
              thumbnail,
              date,
              path,
            } = article.node.frontmatter
            return (
              <PostItem
                title={title}
                key={title}
                brief={brief}
                category=""
                thumbnail={thumbnail}
                date={date}
                readPost={() => {
                  ReactGA.initialize("UA-93856902-6")
                  ReactGA.event({
                    category: "User",
                    action: "Read Post",
                    label: title,
                    value: path,
                    brief,
                  })
                  navigateTo(path)
                }}
              />
            )
          })}
          <img
            className="floating"
            src={R.isNil(african) ? "" : african}
            alt="logo"
          />
        </div>
      </Layout>
    )
  }
}
export const pageQuery = graphql`
  query blogIndex {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: ASC }) {
      edges {
        node {
          frontmatter {
            title
            date
            path
            date
            thumbnail
            brief
          }
        }
      }
    }
  }
`
