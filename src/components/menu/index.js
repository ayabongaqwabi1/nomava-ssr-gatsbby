import React, { Component } from 'react';

import { navigateTo } from 'gatsby-link';

import { slide as Menu } from 'react-burger-menu';

export default class Example extends Component {
  render() {
    console.log('render menu');
    return (
      <Menu
        left
        pageWrapId={'outer-container'}
        outerContainerId={'outer-container'}
      >
        <ul className="col-md-12">
          <li onClick={() => navigateTo('/')}>Home</li>
          <li onClick={() => navigateTo('/posts')}>Posts</li>
          <li onClick={() => navigateTo('/about')}>About</li>
          <li onClick={() => navigateTo('/contact')}>Contact</li>
          <li onClick={() => navigateTo('/disclaimer')}>Disclaimer</li>
          <li onClick={() => navigateTo('/privacypolicy')}>Privacy Policy</li>
          <li onClick={() => navigateTo('/terms')}>Terms of use</li>
          {/* <li className="coming-soon">
            <h4>Coming soon</h4>
            <ul>
              <li>Nomava Tv</li>
              <li>Books</li>
              <li>Podcasts</li>
              <li>Quizes</li>
            </ul>
          </li> */}
        </ul>
      </Menu>
    );
  }
}
