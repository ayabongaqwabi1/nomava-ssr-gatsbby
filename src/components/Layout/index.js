import React, { Component } from 'react';
import { initialize, pageview } from 'react-ga';
import { globalHistory } from '@reach/router';
// import MessengerCustomerChat from 'react-messenger-customer-chat';
import PageSeo from '../PageSeo';
import BurgerMenu from '../menu';
import '../menu/style.scss';
import './global.scss';
import 'bulma/css/bulma.min.css';
export default class Layout extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    initialize('UA-93856902-6');
    pageview(globalHistory.location.href);
  }

  render() {
    const { children, seo } = this.props;
    return (
      <div>
        <div id="fb-root" />
        <PageSeo {...seo} />
        <BurgerMenu />
        <div className="container"> {children} </div>{' '}
      </div>
    );
  }
}
{
  /* <MessengerCustomerChat
          pageId="102482451494182"
          appId="180799756618212"
        /> */
}
{
  /* <AdSense.Google
      client="ca-pub-8294995671791919"
      slot="9157782750"
      style={{ display: 'block' }}
      format="auto"
      responsive="true"
    /> */
}
