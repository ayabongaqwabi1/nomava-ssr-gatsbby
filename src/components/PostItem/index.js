import React, { Component } from "react"
import "./style.scss"
import { isMobile, isBrowser } from "react-device-detect"
import moment from "moment"

import * as R from "ramda"

export default class Post extends Component {
  render() {
    const {
      thumbnail,
      title,
      brief,
      category,
      date,
      side,
      readPost,
    } = this.props
    const likePost = () => {}
    const isLoggedIn = false
    const likes = 0
    const likeButtonText = isMobile ? "" : "Like"
    const PostSizes = isBrowser ? "column is-two-thirds" : "column is-full"
    const postSide = side || "left"
    return (
      <div className={`post ${postSide}-sided ${PostSizes}`}>
        <div className="image">
          <img src={thumbnail} alt="thumbnail" />
        </div>
        <div className="introbox">
          <p className="title">{title}</p>
          <div
            className="intro"
            dangerouslySetInnerHTML={{
              __html: `${R.slice(0, 260, brief)} ...`,
            }}
          />
          <div className="details">
            {isBrowser && <p>{category}</p>}
            <p>{moment(date).format("DD MMM YYYY")}</p>
            <button class="button is-danger" onClick={() => readPost()}>
              Read
            </button>
          </div>
        </div>
      </div>
    )
  }
}
