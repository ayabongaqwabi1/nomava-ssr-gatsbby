import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import { StaticQuery, graphql } from 'gatsby';
import { isNil, isEmpty, replace } from 'ramda';

const SEO = ({ title, description, image, pathname, article }) => (
  <StaticQuery
    query={query}
    render={({
      site: {
        siteMetadata: {
          defaultTitle,
          titleTemplate,
          defaultDescription,
          siteUrl,
          defaultImage,
          twitterUsername,
        },
      },
    }) => {
      const seo = {
        title: title || defaultTitle,
        description: description || defaultDescription,
        image: `${siteUrl}${image || defaultImage}`,
        url: `${siteUrl}${pathname || '/'}`,
      };
      const exists = (i) => !isNil(i) && !isEmpty(i);
      return (
        <Helmet>
          <title>{replace('%s', seo.title, titleTemplate)}</title>
          <meta
            name="title"
            content={replace('%s', seo.title, titleTemplate)}
          />
          <meta
            name="og:title"
            content={replace('%s', seo.title, titleTemplate)}
          />
          {exists(seo.description) && (
            <meta property="og:description" content={seo.description} />
          )}
          {exists(seo.image) && (
            <meta property="og:image" content={seo.image} />
          )}
          {exists(seo.url) && <meta property="og:url" content={seo.url} />}
          {(article ? true : null) && (
            <meta property="og:type" content="article" />
          )}
          <meta property="og:site_name" content="Nomava" />
          <meta name="description" content={seo.description} />
          <meta name="image" content={seo.image} />
          {exists(seo.title) && (
            <meta name="twitter:title" content={seo.title} />
          )}
          {exists(seo.description) && (
            <meta name="twitter:description" content={seo.description} />
          )}
          {exists(seo.image) && (
            <meta name="twitter:image" content={seo.image} />
          )}
          <meta property="fb:app_id" content="180799756618212" />
          <meta property="fb:admins" content="102482451494182" />
          <meta property="og:locale" content="en_UK" />
        </Helmet>
      );
    }}
  />
);

export default SEO;

SEO.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  image: PropTypes.string,
  pathname: PropTypes.string,
  article: PropTypes.bool,
};

SEO.defaultProps = {
  title: null,
  description: null,
  image: null,
  pathname: null,
  article: false,
};

const query = graphql`
  query SEO {
    site {
      siteMetadata {
        defaultTitle: title
        titleTemplate
        defaultDescription: description
        siteUrl
        defaultImage: image
        twitterUsername
      }
    }
  }
`;
