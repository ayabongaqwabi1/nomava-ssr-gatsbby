import React from 'react';
import { graphql } from 'gatsby';
import './style.scss';
import { navigateTo } from 'gatsby-link';
import nl2br from 'nl2br';
import Layout from '../components/Layout';
import { FacebookProvider, Comments, Page } from 'react-facebook';
import ReactGA from 'react-ga';
import {
  EmailShareButton,
  FacebookShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  FacebookIcon,
  TwitterIcon,
  WhatsappIcon,
  EmailIcon,
} from 'react-share';
import ReactFBLike from 'react-fb-like';
import { isMobile, isBrowser } from 'react-device-detect';

export default function Template({ data }) {
  const { markdownRemark } = data; // data.markdownRemark holds our post data
  const { frontmatter, html } = markdownRemark;
  const { title, brief, thumbnail, path } = frontmatter;
  const seo = {
    title,
    description: brief,
    image: thumbnail,
    pathname: path,
    article: true,
  };
  const images = html.match(/\<.*?\>/g).filter((s) => s.includes('img'));
  const divImages = images.map(
    (i) =>
      `<div class="post-image-inside" style="background: url(${/<img[^>]+src="([^">]+)"/g.exec(
        i
      )[1]}) center center no-repeat" alt="thumbnail"></div>`
  );
  String.prototype.replaceArray = function(find, replace) {
    var replaceString = this;
    for (var i = 0; i < find.length; i++) {
      replaceString = replaceString.replace(find[i], replace[i].toString());
    }
    return replaceString;
  };
  const newHtml = html.replaceArray(images, divImages);
  ReactGA.initialize('UA-93856902-6');
  const containerClassSize = isBrowser
    ? 'column is-two-thirds post-text'
    : 'column is-full post-text';
  return (
    <Layout seo={seo}>
      <script
        async
        defer
        crossorigin="anonymous"
        src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v7.0&appId=952015585220162&autoLogAppEvents=1"
        nonce="Z5ROEDl0"
      />
      <div className="wrapper">
        <div className="detail-container">
          <div className="detail text-center">
            <div className="row">
              <div className="column is-full has-text-centered text-left back-to-posts">
                <button
                  class="button is-warning"
                  onClick={() => navigateTo('/posts')}
                >
                  Back to Articles
                </button>
              </div>
            </div>
            <div className={containerClassSize}>
              <div>
                <h1> {`${nl2br(title)}`}</h1>
                <div
                  className="post-image"
                  style={{
                    background: `url(${thumbnail}) center center no-repeat`,
                  }}
                  alt="thumbnail"
                />
                <div
                  className="brief"
                  dangerouslySetInnerHTML={{ __html: nl2br(brief) }}
                />
                <br />
                <div
                  className="article-text"
                  dangerouslySetInnerHTML={{ __html: nl2br(newHtml) }}
                />
                <br />
                <div className="full-width-share col md-12 col-xs-12">
                  <h4> Like, Share & Comment </h4>
                  <br />
                  <FacebookProvider
                    appId="180799756618212"
                    style={{ width: '100%' }}
                    width="100%"
                  >
                    <FacebookShareButton url={`https://nomava.co.za/${path}`}>
                      <FacebookIcon size={32} round={true} />
                    </FacebookShareButton>
                    <TwitterShareButton url={`https://nomava.co.za/${path}`}>
                      <TwitterIcon size={32} round={true} />
                    </TwitterShareButton>
                    <EmailShareButton url={`https://nomava.co.za/${path}`}>
                      <EmailIcon size={32} round={true} />
                    </EmailShareButton>
                    <WhatsappShareButton url={`https://nomava.co.za/${path}`}>
                      <WhatsappIcon size={32} round={true} />
                    </WhatsappShareButton>
                    <ReactFBLike
                      language="en_UK"
                      appId="180799756618212"
                      version="v7"
                      href={`https://nomava.co.za/${path}`}
                      layout="button_count"
                    />
                    <br />

                    <Page
                      href="https://www.facebook.com/nomavainfo"
                      showFacepile
                      adaptContainerWidth
                    />
                    <br />
                    <div
                      class="fb-comments"
                      data-href={`https://nomava.co.za/${path}`}
                      data-numposts="10"
                      data-width=""
                    />
                  </FacebookProvider>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export const pageQuery = graphql`
  query($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        path
        title
        brief
        thumbnail
      }
    }
  }
`;
